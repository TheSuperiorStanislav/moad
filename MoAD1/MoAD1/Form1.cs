﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ZedGraph;

namespace MoAD1
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            GraphPane pane = zedGraph1.GraphPane;
            pane.Title.Text = "Зависимость ошибки классификации от объема выборки";
            pane.YAxis.Title.Text = "Ошибка классификации";
            pane.XAxis.Title.Text = "Объем выборки";
            pane = zedGraph2.GraphPane;
            pane.Title.Text = "Зависимость ошибки классификации от M21 − M11";
            pane.YAxis.Title.Text = "Ошибка классификации";
            pane.XAxis.Title.Text = "M21 − M11";
            pane = zedGraph3.GraphPane;
            pane.Title.Text = "Зависимость ошибки классификации от D21 − D11";
            pane.YAxis.Title.Text = "Ошибка классификации";
            pane.XAxis.Title.Text = "D21 − D11";
            pane = zedGraph4.GraphPane;
            pane.Title.Text = "Зависимость ошибки классификации от P1";
            pane.YAxis.Title.Text = "Ошибка классификации";
            pane.XAxis.Title.Text = "P1";
        }

        private void Launch_Click(object sender, EventArgs e)
        {
            String nl = Environment.NewLine;
            Algorithm newRun = new Algorithm();
            String M11 = textBoxM11.Text, M12 = textBoxM12.Text, M21 = textBoxM21.Text, M22 = textBoxM22.Text;
            String D11 = textBoxD11.Text, D12 = textBoxD12.Text, D21 = textBoxD21.Text, D22 = textBoxD22.Text;
            String P1 = textBoxP1.Text, P2 = textBoxP2.Text;
            String N = textBoxN.Text, K = textBoxK.Text;
            Boolean Flag = checkBoxCPT.Checked;
            try
            {
                if ((Convert.ToDouble(P1) + Convert.ToDouble(P2)) != 1 || Convert.ToDouble(N) == 0 || (Flag && (Convert.ToDouble(K) == 0 || Convert.ToDouble(K) >= 100000000)))
                {
                    MessageBox.Show("Проверьте корректность данных" + nl + "P1 + P2 = 1" + nl + "N != 0 и K != 0", "Ошибка");
                }
                else
                {
                    if (Flag && (Convert.ToDouble(K) > 100 || Convert.ToDouble(K) < 12))
                    {
                        DialogResult dialogResult = MessageBox.Show("Лучше использовать K от 12 до 100(При слишком больших значениях, алгоритм будет очень долго работать)" + nl + "Продолжить?", "Предупреждение", MessageBoxButtons.YesNo);
                        if (dialogResult == DialogResult.Yes)
                        {
                            newRun.DoTheThing(Convert.ToDouble(M11), Convert.ToDouble(M12), Convert.ToDouble(M21), Convert.ToDouble(M22),
                                              Convert.ToDouble(D11), Convert.ToDouble(D12), Convert.ToDouble(D21), Convert.ToDouble(D22),
                                              Convert.ToDouble(P1), Convert.ToDouble(P2), Convert.ToInt32(N), Convert.ToDouble(K), Flag);
                            string[] Result = newRun.GetResults();
                            labelM11.Text = Result[0]; labelM12.Text = Result[1]; labelM21.Text = Result[2]; labelM22.Text = Result[3];
                            labelD11.Text = Result[4]; labelD12.Text = Result[5]; labelD21.Text = Result[6]; labelD22.Text = Result[7];
                            labeln1.Text = Result[8]; labeln2.Text = Result[9]; labelP1.Text = Result[10]; labelP2.Text = Result[11];
                            labelPm1.Text = Result[12]; labelPm2.Text = Result[13]; labelPm.Text = Result[14];
                        }
                    }
                    else
                    {
                        newRun.DoTheThing(Convert.ToDouble(M11), Convert.ToDouble(M12), Convert.ToDouble(M21), Convert.ToDouble(M22),
                                          Convert.ToDouble(D11), Convert.ToDouble(D12), Convert.ToDouble(D21), Convert.ToDouble(D22),
                                          Convert.ToDouble(P1), Convert.ToDouble(P2), Convert.ToInt32(N), Convert.ToDouble(K), Flag);
                        string[] Result = newRun.GetResults();
                        labelM11.Text = Result[0]; labelM12.Text = Result[1]; labelM21.Text = Result[2]; labelM22.Text = Result[3];
                        labelD11.Text = Result[4]; labelD12.Text = Result[5]; labelD21.Text = Result[6]; labelD22.Text = Result[7];
                        labeln1.Text = Result[8]; labeln2.Text = Result[9]; labelP1.Text = Result[10]; labelP2.Text = Result[11];
                        labelPm1.Text = Result[12]; labelPm2.Text = Result[13]; labelPm.Text = Result[14];
                    }
                    DrawGraph(zedGraph1, newRun.GetResultsForFirstGraph());
                    DrawGraph(zedGraph2, newRun.GetResultsForSecondGraph());
                    DrawGraph(zedGraph3, newRun.GetResultsForThirdGraph());
                    DrawGraph(zedGraph4, newRun.GetResultsForThourthGraph());
                }

            }
            catch (FormatException ex)
            {
                MessageBox.Show("Введите числа!!!" + nl + "Ошибка: " + ex.Message, "Ошибка");
            }
            catch (OverflowException ex)
            {
                MessageBox.Show("Слишком большие числа!!!" + nl + "Ошибка: " + ex.Message, "Ошибка");
            }
            catch (Exception ex)
            {
                MessageBox.Show("¯\\_(ツ)_/¯" + nl + ex.Message);
            }


        }

        private void forDouble_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ',') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }


            if ((e.KeyChar == '-') && ((sender as TextBox).Text.IndexOf('-') > -1))
            {
                e.Handled = true;
            }
        }
        private void forDoubleD_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf(',') > -1))
            {
                e.Handled = true;
            }

        }
        private void forInteger_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }
        private void DrawGraph(ZedGraphControl graph,Double[,] Data) 
        {
            graph.GraphPane.CurveList.Clear();
            GraphPane pane = graph.GraphPane;
            PointPairList list = new PointPairList();
            graph.GraphPane.CurveList.Clear();
            Double maxX = Data[0, 0], maxY = Data[1, 0];
            for (int i = 0; i < Data.GetLength(1); i++)
            {
                list.Add(Data[0, i], Data[1, i]);
                if (Data[0, i] > maxX) 
                {
                    maxX = Data[0, i];
                }
                if (Data[1, i] > maxY)
                {
                    maxY = Data[1, i];
                }
            }
            graph.GraphPane.XAxis.Scale.Min = 0;
            graph.GraphPane.XAxis.Scale.Max = maxX;
            graph.GraphPane.YAxis.Scale.Min = 0;
            graph.GraphPane.YAxis.Scale.Max = maxY+0.001;
            LineItem myCurve = pane.AddCurve("", list, Color.Black, SymbolType.None);
            graph.AxisChange();
            graph.Refresh();

        }



    }
}
