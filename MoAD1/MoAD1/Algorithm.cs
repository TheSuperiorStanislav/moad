﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoAD1
{
    class Algorithm
    {
        Double M11, M12, M21, M22;
        Double D11, D12, D21, D22;
        Double M_11, M_12, M_21, M_22;
        Double D_11, D_12, D_21, D_22;
        Double P1, P2;
        Double P_1, P_2;
        int n1, n2, N;
        Double K = 0.0;
        Double Pm1, Pm2, Pm;
        Double[] dots11, dots12, dots21, dots22;
        Boolean Flag;
        Random rand = new Random();
        //Запуск алгоритма
        public void DoTheThing(Double M11, Double M12, Double M21, Double M22,
                               Double D11, Double D12, Double D21, Double D22,
                               Double P1, Double P2, int N, Double K, Boolean Flag)
        {
            this.M11 = M11; this.M12 = M12; this.M21 = M21; this.M22 = M22;

            this.D11 = D11; this.D12 = D12; this.D21 = D21; this.D22 = D22;

            this.P1 = P1; this.P2 = P2; this.N = N; this.K = K; this.Flag = Flag;

            DoAlgorithm();

        }
        Double DoTheThing(int N)
        {

            int oldN = this.N;

            this.N = N;

            DoAlgorithm();

            this.N = oldN;

            return Pm;

        }
        Double DoTheThing(Double A, Double B, Boolean isM)
        {
            Double oldM11 = this.M11, oldM21 = this.M21;
            Double oldD11 = this.D11, oldD21 = this.D21;
            if (isM)
            {
                this.M11 = A;
                this.M21 = B;
            }
            else
            {
                this.D11 = A;
                this.D21 = B;
            }

            DoAlgorithm();

            if (isM)
            {
                this.M11 = oldM11;
                this.M21 = oldM21;
            }
            else
            {
                this.D11 = oldD11;
                this.D21 = oldD21;
            }

            return Pm;
        }
        Double DoTheThing(Double P1)
        {
            Double oldP1 = this.P1;

            Double oldP2 = this.P2;

            this.P1 = P1;

            this.P2 = Math.Round(1.0 - P1,3);

            DoAlgorithm();

            this.P1 = oldP1;

            this.P2 = oldP2;

            return Pm;
        }
        //Алгоритм
        void DoAlgorithm()
        {
            CalcNs();

            P_1 = Convert.ToDouble(n1) / Convert.ToDouble(N);
            P_2 = 1.0 - P_1;

            if (Flag)
            {
                CentralLimitTheorem();
            }
            else
            {
                MethodofPolarCoordinates();
            }

            this.M_11 = CalcM(dots11, n1); this.M_12 = CalcM(dots12, n1); this.M_21 = CalcM(dots21, n2); this.M_22 = CalcM(dots22, n2);

            this.D_11 = CalcD(dots11, M11, n1); this.D_12 = CalcD(dots12, M12, n1); this.D_21 = CalcD(dots21, M21, n2); this.D_22 = CalcD(dots22, M22, n2);

            if (Flag)
            {
                CentralLimitTheorem();
            }
            else
            {
                MethodofPolarCoordinates();
            }

            Pm1 = Classificator(true);

            Pm2 = Classificator(false);

            Pm = Pm1 + Pm2;

        }
        //Расчет обьема
        void CalcNs()
        {
            n1 = 0;
            for (int i = 0; i < N; i++)
            {
                if (rand.NextDouble() < P1)
                    n1++;
            }
            n2 = N - n1;
            dots11 = new Double[n1];
            dots12 = new Double[n1];
            dots21 = new Double[n2];
            dots22 = new Double[n2];
        }
        //Генерация выборки
        void CentralLimitTheorem()
        {
            int countSmall, countBig;
            if (n1 > n2)
            {
                countBig = n1;
                countSmall = n2;
            }
            else
            {
                countBig = n2;
                countSmall = n1;
            }
            for (int i = 0; i < countBig; i++)
            {
                if (i < dots11.Length)
                {
                    dots11[i] = Math.Sqrt(D11) * Math.Sqrt(12.0 / K) * CalcForCLT(rand) + M11;
                    dots12[i] = Math.Sqrt(D12) * Math.Sqrt(12.0 / K) * CalcForCLT(rand) + M12;
                }
                if (i < dots21.Length)
                {
                    dots21[i] = Math.Sqrt(D21) * Math.Sqrt(12.0 / K) * CalcForCLT(rand) + M21;
                    dots22[i] = Math.Sqrt(D22) * Math.Sqrt(12.0 / K) * CalcForCLT(rand) + M22;
                }
            }
        }
        Double CalcForCLT(Random rand)
        {

            Double Result = 0.0;
            for (int i = 0; i < K; i++)
            {
                Result += (rand.NextDouble() - 0.5);
            }
            return Result;
        }
        void MethodofPolarCoordinates()
        {
            Double U1, V1, U2, V2, S, X1, X2;
            int countBig;
            if (n1 > n2)
            {
                countBig = n1;
            }
            else
            {
                countBig = n2;
            }
            for (int i = 0; i < countBig; i++)
            {
                do
                {
                    U1 = rand.NextDouble();
                    U2 = rand.NextDouble();
                    V1 = 2 * U1 - 1;
                    V2 = 2 * U2 - 1;
                    S = V1 * V1 + V2 * V2;
                } while (S >= 1);
                X1 = V1 * Math.Sqrt(-2 * Math.Log(S) / S);
                X2 = V2 * Math.Sqrt(-2 * Math.Log(S) / S);
                if (i < dots11.Length)
                {
                    dots11[i] = Math.Sqrt(D11) * X1 + M11;
                    dots12[i] = Math.Sqrt(D12) * X2 + M12;
                }
                if (i < dots21.Length)
                {
                    dots21[i] = Math.Sqrt(D21) * X1 + M21;
                    dots22[i] = Math.Sqrt(D22) * X2 + M22;
                }
            }
        }
        //Вычесления (Мат.ожидание, дисперсия, Классификатор, решающия функция)
        Double CalcM(double[] dots, int n)
        {
            Double Result = 0.0;
            for (int i = 0; i < n; i++)
            {
                Result += dots[i];
            }
            return Result / Convert.ToDouble(n);
        }
        Double CalcD(double[] dots, Double M, int n)
        {
            Double Result = 0.0;
            for (int i = 0; i < n; i++)
            {
                Result += Math.Pow(dots[i] - M, 2);
            }
            return Result / Convert.ToDouble(n);
        }
        Double Classificator(Boolean Class)
        {
            Double Result = 0.0;
            double DecisionF;

            if (Class)
            {
                for (int i = 0; i < n1; i++)
                {
                    DecisionF = F(dots11[i], dots12[i], M_11, M_12, D_11, D_12) * P_1
                              - F(dots11[i], dots12[i], M_21, M_22, D_21, D_22) * P_2;

                    if (DecisionF < 0)
                        Result++;
                }
            }
            else
            {
                for (int i = 0; i < n2; i++)
                {
                    DecisionF = F(dots21[i], dots22[i], M_11, M_12, D_11, D_12) * P_1
                              - F(dots21[i], dots22[i], M_21, M_22, D_21, D_22) * P_2;

                    if (DecisionF > 0)
                        Result++;
                }
            }
            Result /= Convert.ToDouble(N);
            return Result;
        }
        Double F(Double X1, Double X2, Double M1, Double M2, Double D1, Double D2)
        {
            Double Result = Math.Exp(-0.5 *
                (Math.Pow((X1 - M1) / Math.Sqrt(D1), 2.0) +
                 Math.Pow((X2 - M2) / Math.Sqrt(D2), 2.0))) /
                 (2.0 * Math.PI*Math.Sqrt(D1 * D2));
            return Result;
        }
        //Получение результатов
        public string[] GetResults()
        {
            String str = "";
            str += Math.Round(M_11, 3) + "#" + Math.Round(M_12, 3) + "#" + Math.Round(M_21, 3) + "#" + Math.Round(M_22, 3) + "#" +
                   Math.Round(D_11, 3) + "#" + Math.Round(D_12, 3) + "#" + Math.Round(D_21, 3) + "#" + Math.Round(D_22, 3) + "#" +
                   n1 + "#" + n2 + "#" + P_1 + "#" + P_2 + "#" + Pm1 + "#" + Pm2 + "#" + Pm;
            string[] Data = str.Split('#');
            return Data;
        }
        public Double[,] GetResultsForFirstGraph()
        {
            Double[,] Data = new Double[2, 900];
            for (int i = 0; i < 900; i++)
            {
                Data[0, i] = (i + 100);
                //for (int j = 0; j < 90; j++)
                    Data[1, i] = DoTheThing(i + 100);
            }
            return Data;
        }
        public Double[,] GetResultsForSecondGraph()
        {
            Double[,] Data = new Double[2, Convert.ToInt32(Math.Abs(M21 - M11) / 0.01) + 1];
            int count = 0;
            if (M21 > M11)
            {
                for (Double i = M11; i < M21; i += 0.01)
                {
                    Data[0, count] = Math.Abs(M21 - i);
                    //for (int j = 0; j < 50; j++)
                        Data[1, count] = DoTheThing(i, M21, true);
                    count++;
                }
            }
            else
            {
                for (Double i = M21; i < M11; i += 0.01)
                {
                    Data[0, count] = Math.Abs(M11 - i);
                    //for (int j = 0; j < 50; j++)
                        Data[1, count] = DoTheThing(M11, i, true);
                    count++;
                }
            }
            return Data;
        }
        public Double[,] GetResultsForThirdGraph()
        {
            Double[,] Data = new Double[2, Convert.ToInt32(Math.Abs(D21 - D11) / 0.01) + 1];
            int count = 0;
            if (D21 > D11)
            {
                for (Double i = D11; i < D21; i += 0.01)
                {
                    Data[0, count] = Math.Abs(D21 - i);
                    //for (int j = 0; j < 100; j++)
                        Data[1, count] = DoTheThing(i, D21, false);
                    count++;
                }
            }
            else
            {
                for (Double i = D21; i < D11; i += 0.01)
                {
                    Data[0, count] = Math.Abs(D11 - i);
                    //for (int j = 0; j < 100; j++)
                        Data[1, count] = DoTheThing(D11, i, false);
                    count++;
                }
            }
            return Data;
        }
        public Double[,] GetResultsForThourthGraph()
        {
            Double[,] Data = new Double[2, Convert.ToInt32(1.0 / 0.1+1.0)];
            int count = 0;
            for (Double i = 0; Math.Round(i,3) < 1.1; i += 0.1)
            {
                Data[0, count] = i;
                Double temp = 0.0;
                for (int j = 0; j < 25; j++)
                    temp+=  DoTheThing(Math.Round(i,3));
                Data[1, count] = temp/25.0;
                count++;
            }

            return Data;
        }

    }

}
