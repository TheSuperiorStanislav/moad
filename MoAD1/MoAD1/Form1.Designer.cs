﻿namespace MoAD1
{
    partial class MainForm
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.textBoxM11 = new System.Windows.Forms.TextBox();
            this.textBoxM12 = new System.Windows.Forms.TextBox();
            this.textBoxD11 = new System.Windows.Forms.TextBox();
            this.textBoxD12 = new System.Windows.Forms.TextBox();
            this.textBoxM21 = new System.Windows.Forms.TextBox();
            this.textBoxM22 = new System.Windows.Forms.TextBox();
            this.textBoxD21 = new System.Windows.Forms.TextBox();
            this.textBoxD22 = new System.Windows.Forms.TextBox();
            this.textBoxP1 = new System.Windows.Forms.TextBox();
            this.textBoxP2 = new System.Windows.Forms.TextBox();
            this.textBoxN = new System.Windows.Forms.TextBox();
            this.textBoxK = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.labelM11 = new System.Windows.Forms.Label();
            this.labelM12 = new System.Windows.Forms.Label();
            this.labelD12 = new System.Windows.Forms.Label();
            this.labelD11 = new System.Windows.Forms.Label();
            this.labelPm1 = new System.Windows.Forms.Label();
            this.labelP1 = new System.Windows.Forms.Label();
            this.labeln1 = new System.Windows.Forms.Label();
            this.labelPm2 = new System.Windows.Forms.Label();
            this.labelP2 = new System.Windows.Forms.Label();
            this.labeln2 = new System.Windows.Forms.Label();
            this.labelD22 = new System.Windows.Forms.Label();
            this.labelD21 = new System.Windows.Forms.Label();
            this.labelM22 = new System.Windows.Forms.Label();
            this.labelM21 = new System.Windows.Forms.Label();
            this.labelPm = new System.Windows.Forms.Label();
            this.Launch = new System.Windows.Forms.Button();
            this.checkBoxCPT = new System.Windows.Forms.CheckBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.zedGraph1 = new ZedGraph.ZedGraphControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.zedGraph2 = new ZedGraph.ZedGraphControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.zedGraph3 = new ZedGraph.ZedGraphControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.zedGraph4 = new ZedGraph.ZedGraphControl();
            this.label15 = new System.Windows.Forms.Label();
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(15, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Исходные данные";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(29, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Первый класс";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(218, 49);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Второй класс";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(29, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "M11";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(29, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "M12";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(29, 159);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(37, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "D12";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(29, 132);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(37, 17);
            this.label7.TabIndex = 5;
            this.label7.Text = "D11";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(218, 159);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 17);
            this.label8.TabIndex = 10;
            this.label8.Text = "D22";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(218, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(37, 17);
            this.label9.TabIndex = 9;
            this.label9.Text = "D21";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(218, 105);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(38, 17);
            this.label10.TabIndex = 8;
            this.label10.Text = "M22";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.Location = new System.Drawing.Point(218, 78);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 17);
            this.label11.TabIndex = 7;
            this.label11.Text = "M21";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(218, 215);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(32, 17);
            this.label12.TabIndex = 12;
            this.label12.Text = "P2:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(29, 215);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(32, 17);
            this.label13.TabIndex = 11;
            this.label13.Text = "P1:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(29, 251);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(19, 17);
            this.label14.TabIndex = 13;
            this.label14.Text = "N";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(29, 336);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(18, 17);
            this.label16.TabIndex = 15;
            this.label16.Text = "K";
            // 
            // textBoxM11
            // 
            this.textBoxM11.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxM11.Location = new System.Drawing.Point(74, 78);
            this.textBoxM11.Name = "textBoxM11";
            this.textBoxM11.Size = new System.Drawing.Size(100, 20);
            this.textBoxM11.TabIndex = 16;
            this.textBoxM11.Text = "10";
            this.textBoxM11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxM12
            // 
            this.textBoxM12.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxM12.Location = new System.Drawing.Point(74, 104);
            this.textBoxM12.Name = "textBoxM12";
            this.textBoxM12.Size = new System.Drawing.Size(100, 20);
            this.textBoxM12.TabIndex = 17;
            this.textBoxM12.Text = "10";
            this.textBoxM12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxD11
            // 
            this.textBoxD11.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxD11.Location = new System.Drawing.Point(74, 132);
            this.textBoxD11.Name = "textBoxD11";
            this.textBoxD11.Size = new System.Drawing.Size(100, 20);
            this.textBoxD11.TabIndex = 18;
            this.textBoxD11.Text = "2";
            this.textBoxD11.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDoubleD_KeyPress);
            // 
            // textBoxD12
            // 
            this.textBoxD12.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxD12.Location = new System.Drawing.Point(74, 159);
            this.textBoxD12.Name = "textBoxD12";
            this.textBoxD12.Size = new System.Drawing.Size(100, 20);
            this.textBoxD12.TabIndex = 19;
            this.textBoxD12.Text = "2";
            this.textBoxD12.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDoubleD_KeyPress);
            // 
            // textBoxM21
            // 
            this.textBoxM21.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxM21.Location = new System.Drawing.Point(262, 78);
            this.textBoxM21.Name = "textBoxM21";
            this.textBoxM21.Size = new System.Drawing.Size(100, 20);
            this.textBoxM21.TabIndex = 20;
            this.textBoxM21.Text = "15";
            this.textBoxM21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxM22
            // 
            this.textBoxM22.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxM22.Location = new System.Drawing.Point(262, 105);
            this.textBoxM22.Name = "textBoxM22";
            this.textBoxM22.Size = new System.Drawing.Size(100, 20);
            this.textBoxM22.TabIndex = 21;
            this.textBoxM22.Text = "15";
            this.textBoxM22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxD21
            // 
            this.textBoxD21.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxD21.Location = new System.Drawing.Point(262, 132);
            this.textBoxD21.Name = "textBoxD21";
            this.textBoxD21.Size = new System.Drawing.Size(100, 20);
            this.textBoxD21.TabIndex = 22;
            this.textBoxD21.Text = "3";
            this.textBoxD21.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDoubleD_KeyPress);
            // 
            // textBoxD22
            // 
            this.textBoxD22.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxD22.Location = new System.Drawing.Point(262, 159);
            this.textBoxD22.Name = "textBoxD22";
            this.textBoxD22.Size = new System.Drawing.Size(100, 20);
            this.textBoxD22.TabIndex = 23;
            this.textBoxD22.Text = "3";
            this.textBoxD22.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDoubleD_KeyPress);
            // 
            // textBoxP1
            // 
            this.textBoxP1.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxP1.Location = new System.Drawing.Point(74, 211);
            this.textBoxP1.Name = "textBoxP1";
            this.textBoxP1.Size = new System.Drawing.Size(100, 20);
            this.textBoxP1.TabIndex = 24;
            this.textBoxP1.Text = "0,5";
            this.textBoxP1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxP2
            // 
            this.textBoxP2.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxP2.Location = new System.Drawing.Point(262, 214);
            this.textBoxP2.Name = "textBoxP2";
            this.textBoxP2.Size = new System.Drawing.Size(100, 20);
            this.textBoxP2.TabIndex = 25;
            this.textBoxP2.Text = "0,5";
            this.textBoxP2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forDouble_KeyPress);
            // 
            // textBoxN
            // 
            this.textBoxN.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxN.Location = new System.Drawing.Point(74, 248);
            this.textBoxN.Name = "textBoxN";
            this.textBoxN.Size = new System.Drawing.Size(100, 20);
            this.textBoxN.TabIndex = 26;
            this.textBoxN.Text = "500";
            this.textBoxN.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forInteger_KeyPress);
            // 
            // textBoxK
            // 
            this.textBoxK.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.textBoxK.Location = new System.Drawing.Point(74, 333);
            this.textBoxK.Name = "textBoxK";
            this.textBoxK.Size = new System.Drawing.Size(100, 20);
            this.textBoxK.TabIndex = 27;
            this.textBoxK.Text = "100";
            this.textBoxK.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.forInteger_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label18.Location = new System.Drawing.Point(672, 214);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(40, 17);
            this.label18.TabIndex = 40;
            this.label18.Text = "^P2:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label19.Location = new System.Drawing.Point(413, 215);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(40, 17);
            this.label19.TabIndex = 39;
            this.label19.Text = "^P1:";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label20.Location = new System.Drawing.Point(667, 159);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(45, 17);
            this.label20.TabIndex = 38;
            this.label20.Text = "^D22";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label21.Location = new System.Drawing.Point(667, 132);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(45, 17);
            this.label21.TabIndex = 37;
            this.label21.Text = "^D21";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label22.Location = new System.Drawing.Point(667, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(46, 17);
            this.label22.TabIndex = 36;
            this.label22.Text = "^M22";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label23.Location = new System.Drawing.Point(667, 78);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 17);
            this.label23.TabIndex = 35;
            this.label23.Text = "^M21";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label24.Location = new System.Drawing.Point(413, 159);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(45, 17);
            this.label24.TabIndex = 34;
            this.label24.Text = "^D12";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label25.Location = new System.Drawing.Point(413, 132);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(45, 17);
            this.label25.TabIndex = 33;
            this.label25.Text = "^D11";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label26.Location = new System.Drawing.Point(413, 105);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(46, 17);
            this.label26.TabIndex = 32;
            this.label26.Text = "^M12";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label27.Location = new System.Drawing.Point(413, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(46, 17);
            this.label27.TabIndex = 31;
            this.label27.Text = "^M11";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label28.Location = new System.Drawing.Point(667, 49);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(109, 17);
            this.label28.TabIndex = 30;
            this.label28.Text = "Второй класс";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label29.Location = new System.Drawing.Point(413, 49);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(112, 17);
            this.label29.TabIndex = 29;
            this.label29.Text = "Первый класс";
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label.Location = new System.Drawing.Point(673, 188);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(31, 17);
            this.label.TabIndex = 42;
            this.label.Text = "n2:";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label31.Location = new System.Drawing.Point(414, 189);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(31, 17);
            this.label31.TabIndex = 41;
            this.label31.Text = "n1:";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label32.Location = new System.Drawing.Point(673, 241);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(56, 17);
            this.label32.TabIndex = 44;
            this.label32.Text = "^Pош2";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label33.Location = new System.Drawing.Point(414, 242);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(56, 17);
            this.label33.TabIndex = 43;
            this.label33.Text = "^Pош1";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label30.Location = new System.Drawing.Point(568, 297);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(47, 17);
            this.label30.TabIndex = 45;
            this.label30.Text = "^Pош";
            // 
            // labelM11
            // 
            this.labelM11.AutoSize = true;
            this.labelM11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelM11.Location = new System.Drawing.Point(508, 78);
            this.labelM11.Name = "labelM11";
            this.labelM11.Size = new System.Drawing.Size(17, 17);
            this.labelM11.TabIndex = 46;
            this.labelM11.Text = "0";
            // 
            // labelM12
            // 
            this.labelM12.AutoSize = true;
            this.labelM12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelM12.Location = new System.Drawing.Point(508, 105);
            this.labelM12.Name = "labelM12";
            this.labelM12.Size = new System.Drawing.Size(17, 17);
            this.labelM12.TabIndex = 47;
            this.labelM12.Text = "0";
            // 
            // labelD12
            // 
            this.labelD12.AutoSize = true;
            this.labelD12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelD12.Location = new System.Drawing.Point(508, 159);
            this.labelD12.Name = "labelD12";
            this.labelD12.Size = new System.Drawing.Size(17, 17);
            this.labelD12.TabIndex = 49;
            this.labelD12.Text = "0";
            // 
            // labelD11
            // 
            this.labelD11.AutoSize = true;
            this.labelD11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelD11.Location = new System.Drawing.Point(508, 132);
            this.labelD11.Name = "labelD11";
            this.labelD11.Size = new System.Drawing.Size(17, 17);
            this.labelD11.TabIndex = 48;
            this.labelD11.Text = "0";
            // 
            // labelPm1
            // 
            this.labelPm1.AutoSize = true;
            this.labelPm1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPm1.Location = new System.Drawing.Point(508, 243);
            this.labelPm1.Name = "labelPm1";
            this.labelPm1.Size = new System.Drawing.Size(17, 17);
            this.labelPm1.TabIndex = 52;
            this.labelPm1.Text = "0";
            // 
            // labelP1
            // 
            this.labelP1.AutoSize = true;
            this.labelP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelP1.Location = new System.Drawing.Point(508, 216);
            this.labelP1.Name = "labelP1";
            this.labelP1.Size = new System.Drawing.Size(17, 17);
            this.labelP1.TabIndex = 51;
            this.labelP1.Text = "0";
            // 
            // labeln1
            // 
            this.labeln1.AutoSize = true;
            this.labeln1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labeln1.Location = new System.Drawing.Point(508, 189);
            this.labeln1.Name = "labeln1";
            this.labeln1.Size = new System.Drawing.Size(17, 17);
            this.labeln1.TabIndex = 50;
            this.labeln1.Text = "0";
            // 
            // labelPm2
            // 
            this.labelPm2.AutoSize = true;
            this.labelPm2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPm2.Location = new System.Drawing.Point(759, 244);
            this.labelPm2.Name = "labelPm2";
            this.labelPm2.Size = new System.Drawing.Size(17, 17);
            this.labelPm2.TabIndex = 59;
            this.labelPm2.Text = "0";
            // 
            // labelP2
            // 
            this.labelP2.AutoSize = true;
            this.labelP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelP2.Location = new System.Drawing.Point(759, 217);
            this.labelP2.Name = "labelP2";
            this.labelP2.Size = new System.Drawing.Size(17, 17);
            this.labelP2.TabIndex = 58;
            this.labelP2.Text = "0";
            // 
            // labeln2
            // 
            this.labeln2.AutoSize = true;
            this.labeln2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labeln2.Location = new System.Drawing.Point(759, 190);
            this.labeln2.Name = "labeln2";
            this.labeln2.Size = new System.Drawing.Size(17, 17);
            this.labeln2.TabIndex = 57;
            this.labeln2.Text = "0";
            // 
            // labelD22
            // 
            this.labelD22.AutoSize = true;
            this.labelD22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelD22.Location = new System.Drawing.Point(759, 160);
            this.labelD22.Name = "labelD22";
            this.labelD22.Size = new System.Drawing.Size(17, 17);
            this.labelD22.TabIndex = 56;
            this.labelD22.Text = "0";
            // 
            // labelD21
            // 
            this.labelD21.AutoSize = true;
            this.labelD21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelD21.Location = new System.Drawing.Point(759, 133);
            this.labelD21.Name = "labelD21";
            this.labelD21.Size = new System.Drawing.Size(17, 17);
            this.labelD21.TabIndex = 55;
            this.labelD21.Text = "0";
            // 
            // labelM22
            // 
            this.labelM22.AutoSize = true;
            this.labelM22.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelM22.Location = new System.Drawing.Point(759, 106);
            this.labelM22.Name = "labelM22";
            this.labelM22.Size = new System.Drawing.Size(17, 17);
            this.labelM22.TabIndex = 54;
            this.labelM22.Text = "0";
            // 
            // labelM21
            // 
            this.labelM21.AutoSize = true;
            this.labelM21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelM21.Location = new System.Drawing.Point(759, 79);
            this.labelM21.Name = "labelM21";
            this.labelM21.Size = new System.Drawing.Size(17, 17);
            this.labelM21.TabIndex = 53;
            this.labelM21.Text = "0";
            // 
            // labelPm
            // 
            this.labelPm.AutoSize = true;
            this.labelPm.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelPm.Location = new System.Drawing.Point(632, 297);
            this.labelPm.Name = "labelPm";
            this.labelPm.Size = new System.Drawing.Size(17, 17);
            this.labelPm.TabIndex = 60;
            this.labelPm.Text = "0";
            // 
            // Launch
            // 
            this.Launch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Launch.Location = new System.Drawing.Point(239, 318);
            this.Launch.Name = "Launch";
            this.Launch.Size = new System.Drawing.Size(123, 35);
            this.Launch.TabIndex = 61;
            this.Launch.Text = "Расчет";
            this.Launch.UseMnemonic = false;
            this.Launch.UseVisualStyleBackColor = true;
            this.Launch.Click += new System.EventHandler(this.Launch_Click);
            // 
            // checkBoxCPT
            // 
            this.checkBoxCPT.AutoSize = true;
            this.checkBoxCPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.checkBoxCPT.Location = new System.Drawing.Point(32, 297);
            this.checkBoxCPT.Name = "checkBoxCPT";
            this.checkBoxCPT.Size = new System.Drawing.Size(169, 21);
            this.checkBoxCPT.TabIndex = 63;
            this.checkBoxCPT.Text = "Использовать ЦПТ";
            this.checkBoxCPT.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Location = new System.Drawing.Point(7, 416);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(991, 462);
            this.tabControl1.TabIndex = 64;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.zedGraph1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(983, 436);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Зависимость ошибки классификации от объема выборки";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // zedGraph1
            // 
            this.zedGraph1.IsEnableHPan = false;
            this.zedGraph1.IsEnableHZoom = false;
            this.zedGraph1.IsEnableVPan = false;
            this.zedGraph1.IsEnableVZoom = false;
            this.zedGraph1.IsPrintFillPage = false;
            this.zedGraph1.IsPrintKeepAspectRatio = false;
            this.zedGraph1.IsPrintScaleAll = false;
            this.zedGraph1.IsShowContextMenu = false;
            this.zedGraph1.IsShowCopyMessage = false;
            this.zedGraph1.IsSynchronizeYAxes = true;
            this.zedGraph1.Location = new System.Drawing.Point(-4, 0);
            this.zedGraph1.Name = "zedGraph1";
            this.zedGraph1.ScrollGrace = 0D;
            this.zedGraph1.ScrollMaxX = 1000D;
            this.zedGraph1.ScrollMaxY = 1D;
            this.zedGraph1.ScrollMaxY2 = 0D;
            this.zedGraph1.ScrollMinX = 0D;
            this.zedGraph1.ScrollMinY = 0D;
            this.zedGraph1.ScrollMinY2 = 0D;
            this.zedGraph1.Size = new System.Drawing.Size(991, 440);
            this.zedGraph1.TabIndex = 0;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.zedGraph2);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(983, 436);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Зависимость ошибки классификации от M21 − M11";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // zedGraph2
            // 
            this.zedGraph2.IsEnableHPan = false;
            this.zedGraph2.IsEnableHZoom = false;
            this.zedGraph2.IsEnableVPan = false;
            this.zedGraph2.IsEnableVZoom = false;
            this.zedGraph2.IsEnableWheelZoom = false;
            this.zedGraph2.IsPrintFillPage = false;
            this.zedGraph2.IsPrintKeepAspectRatio = false;
            this.zedGraph2.IsPrintScaleAll = false;
            this.zedGraph2.IsShowContextMenu = false;
            this.zedGraph2.IsShowCopyMessage = false;
            this.zedGraph2.Location = new System.Drawing.Point(-4, 0);
            this.zedGraph2.Name = "zedGraph2";
            this.zedGraph2.ScrollGrace = 0D;
            this.zedGraph2.ScrollMaxX = 0D;
            this.zedGraph2.ScrollMaxY = 0D;
            this.zedGraph2.ScrollMaxY2 = 0D;
            this.zedGraph2.ScrollMinX = 0D;
            this.zedGraph2.ScrollMinY = 0D;
            this.zedGraph2.ScrollMinY2 = 0D;
            this.zedGraph2.Size = new System.Drawing.Size(991, 440);
            this.zedGraph2.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.zedGraph3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(983, 436);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Зависимость ошибки классификации от D21 − D11";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // zedGraph3
            // 
            this.zedGraph3.IsEnableHPan = false;
            this.zedGraph3.IsEnableHZoom = false;
            this.zedGraph3.IsEnableVPan = false;
            this.zedGraph3.IsEnableVZoom = false;
            this.zedGraph3.IsEnableWheelZoom = false;
            this.zedGraph3.IsPrintFillPage = false;
            this.zedGraph3.IsPrintKeepAspectRatio = false;
            this.zedGraph3.IsPrintScaleAll = false;
            this.zedGraph3.IsShowContextMenu = false;
            this.zedGraph3.IsShowCopyMessage = false;
            this.zedGraph3.Location = new System.Drawing.Point(-4, 0);
            this.zedGraph3.Name = "zedGraph3";
            this.zedGraph3.ScrollGrace = 0D;
            this.zedGraph3.ScrollMaxX = 0D;
            this.zedGraph3.ScrollMaxY = 0D;
            this.zedGraph3.ScrollMaxY2 = 0D;
            this.zedGraph3.ScrollMinX = 0D;
            this.zedGraph3.ScrollMinY = 0D;
            this.zedGraph3.ScrollMinY2 = 0D;
            this.zedGraph3.Size = new System.Drawing.Size(991, 440);
            this.zedGraph3.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.zedGraph4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(983, 436);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Зависимость ошибки классификации от P1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // zedGraph4
            // 
            this.zedGraph4.IsEnableHPan = false;
            this.zedGraph4.IsEnableHZoom = false;
            this.zedGraph4.IsEnableVPan = false;
            this.zedGraph4.IsEnableVZoom = false;
            this.zedGraph4.IsPrintFillPage = false;
            this.zedGraph4.IsPrintKeepAspectRatio = false;
            this.zedGraph4.IsPrintScaleAll = false;
            this.zedGraph4.IsShowContextMenu = false;
            this.zedGraph4.IsShowCopyMessage = false;
            this.zedGraph4.IsSynchronizeYAxes = true;
            this.zedGraph4.Location = new System.Drawing.Point(-4, 0);
            this.zedGraph4.Name = "zedGraph4";
            this.zedGraph4.ScrollGrace = 0D;
            this.zedGraph4.ScrollMaxX = 1000D;
            this.zedGraph4.ScrollMaxY = 1D;
            this.zedGraph4.ScrollMaxY2 = 0D;
            this.zedGraph4.ScrollMinX = 0D;
            this.zedGraph4.ScrollMinY = 0D;
            this.zedGraph4.ScrollMinY2 = 0D;
            this.zedGraph4.Size = new System.Drawing.Size(991, 440);
            this.zedGraph4.TabIndex = 1;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(15, 384);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(73, 17);
            this.label15.TabIndex = 65;
            this.label15.Text = "Графики";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1004, 886);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.Launch);
            this.Controls.Add(this.checkBoxCPT);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.labelPm);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.labelPm2);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.labelP2);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.textBoxK);
            this.Controls.Add(this.labeln2);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.textBoxN);
            this.Controls.Add(this.labelD22);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.textBoxP2);
            this.Controls.Add(this.labelD21);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.textBoxP1);
            this.Controls.Add(this.labelM22);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.textBoxD22);
            this.Controls.Add(this.labelM21);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.textBoxD21);
            this.Controls.Add(this.labelPm1);
            this.Controls.Add(this.label);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.textBoxM22);
            this.Controls.Add(this.labelP1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.textBoxM21);
            this.Controls.Add(this.labeln1);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.textBoxD12);
            this.Controls.Add(this.labelD12);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.textBoxD11);
            this.Controls.Add(this.labelD11);
            this.Controls.Add(this.labelM11);
            this.Controls.Add(this.textBoxM11);
            this.Controls.Add(this.textBoxM12);
            this.Controls.Add(this.labelM12);
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "МАД: Лабораторная работа №1: Классификаци в распознавании образов";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBoxM11;
        private System.Windows.Forms.TextBox textBoxM12;
        private System.Windows.Forms.TextBox textBoxD11;
        private System.Windows.Forms.TextBox textBoxD12;
        private System.Windows.Forms.TextBox textBoxM21;
        private System.Windows.Forms.TextBox textBoxM22;
        private System.Windows.Forms.TextBox textBoxD21;
        private System.Windows.Forms.TextBox textBoxD22;
        private System.Windows.Forms.TextBox textBoxP1;
        private System.Windows.Forms.TextBox textBoxP2;
        private System.Windows.Forms.TextBox textBoxN;
        private System.Windows.Forms.TextBox textBoxK;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label labelM11;
        private System.Windows.Forms.Label labelM12;
        private System.Windows.Forms.Label labelD12;
        private System.Windows.Forms.Label labelD11;
        private System.Windows.Forms.Label labelPm1;
        private System.Windows.Forms.Label labelP1;
        private System.Windows.Forms.Label labeln1;
        private System.Windows.Forms.Label labelPm2;
        private System.Windows.Forms.Label labelP2;
        private System.Windows.Forms.Label labeln2;
        private System.Windows.Forms.Label labelD22;
        private System.Windows.Forms.Label labelD21;
        private System.Windows.Forms.Label labelM22;
        private System.Windows.Forms.Label labelM21;
        private System.Windows.Forms.Label labelPm;
        private System.Windows.Forms.Button Launch;
        private System.Windows.Forms.CheckBox checkBoxCPT;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage2;
        private ZedGraph.ZedGraphControl zedGraph1;
        private System.Windows.Forms.TabPage tabPage3;
        private ZedGraph.ZedGraphControl zedGraph2;
        private System.Windows.Forms.TabPage tabPage4;
        private ZedGraph.ZedGraphControl zedGraph3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TabPage tabPage1;
        private ZedGraph.ZedGraphControl zedGraph4;
    }
}

