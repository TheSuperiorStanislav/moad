package com.skhlud.android.moad4

import net.objecthunter.exp4j.ExpressionBuilder
import java.util.*

/**
 * Created by 1 on 16.05.2017.
 */
class ModelBuilder {
    class Point(u1: Double , u2: Double) {
        val u1 = u1 //вход 1
        val u2 = u2 //вход 2
        val x = Model.Func(u1,u2,true) //выход обькта
        var _x =0.0 // выход модели
        val x1 = Model.functionU1.setVariable("u", u1).evaluate() //для метода крамера
        val x2 = Model.functionU2.setVariable("u", u2).evaluate() //для метода крамера
    }

    object Model{
        var randomForDots = Random()
        var randomForInterference = Random()
        var n = 0
        var mExp = 0.0 // мат. ожидание
        var disp = 0.0 // диспресия
        var a1 = 0.0 // а1 лол
        var functionU1 = ExpressionBuilder("u").variables("u").build()!! // первая функция
        var a2 = 0.0 // а2 лол
        var functionU2 = ExpressionBuilder("u").variables("u").build()!! // вторая функция

        var altha1 = 0.0
        var altha2 = 0.0
        var dots = emptyArray<Point>() // данные для таблицы

        var isDone = false

        fun DoTheThing(n: Int,mExp: Double,disp :Double,a1 : Double,functionU1 : String,a2 : Double,functionU2 : String) {
            isDone = false
            this.randomForDots = Random()
            this.randomForInterference = Random()
            this.n = n
            this.mExp = mExp
            this.disp = disp
            this.a1 = a1
            this.functionU1 = ExpressionBuilder(functionU1).variables("u").build()
            this.a2 = a2
            this.functionU2 = ExpressionBuilder(functionU2).variables("u").build()

            dots = Array(n,{_ -> Point(GenerateU(),GenerateU())}) //
            altha1 = 0.0
            altha2 = 0.0
            FindAlthas()
            for (i in 0..n-1) {
                dots[i]._x= Model.Func(dots[i].u1,dots[i].u2,false)
            }
            isDone = true
        }
        //Поиск коф. методом см. файл forMoAD4.docx
        fun FindAlthas() {
            var A1 = 0.0
            var A2 = 0.0
            var B1 = 0.0
            var B2 = 0.0
            var C1 = 0.0
            var C2 = 0.0
            for (i in 0..n-1) {
                A1 += Math.pow(dots[i].x1, 2.0);A2 += dots[i].x1 * dots[i].x2
                B1 += dots[i].x1 * dots[i].x2;B2 += Math.pow(dots[i].x2, 2.0)
                C1 += dots[i].x1 * dots[i].x;C2 += dots[i].x2 * dots[i].x
            }
            val determinant = A1 * B2 - A2 * B1
            altha1 = (C1 * B2 - C2 * B1)/determinant
            altha2 = (A1 * C2 - A2 * C1)/determinant
        }
        //генерация выыходов, нам сказали сделать от 0 до 3
        fun GenerateU():Double = 3 * randomForDots.nextDouble()
        //Сама функция
        fun Func(u1:Double, u2: Double,flag: Boolean):Double {
            functionU1.setVariable("u", u1)
            functionU2.setVariable("u", u2)
            if (flag)
                return a1 * functionU1.evaluate() + a2 * functionU2.evaluate() + CentralLimitTheorem()
            else
                return altha1 * functionU1.evaluate() + altha2 * functionU2.evaluate() + CentralLimitTheorem()
        }
        //Помеха
        fun CentralLimitTheorem(): Double = Math.sqrt(disp) * Math.sqrt(12.0 / 100) * CalcForCLT() + mExp
        fun CalcForCLT(): Double {

            var Result = 0.0
            for (i in 0..99) {
                Result += randomForInterference.nextDouble() - 0.5
            }
            return Result
        }
    }
}