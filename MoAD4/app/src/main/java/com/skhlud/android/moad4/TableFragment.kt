package com.skhlud.android.moad4

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import android.widget.ListView
import org.jetbrains.annotations.Nullable


class TableFragment : Fragment(){

    @Nullable
    override fun onCreateView(inflater: LayoutInflater?, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view: View
        if (ModelBuilder.Model.isDone) {
            view = inflater!!.inflate(R.layout.fragment_table, container, false)
        } else {
            view = inflater!!.inflate(R.layout.fragment_oh_no, container, false)
        }
        return view
    }


    override fun onViewCreated(view: View?, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (ModelBuilder.Model.isDone) {
            val arrayList = ArrayList<ModelBuilder.Point>(ModelBuilder.Model.dots.asList())
            val adapter = ListAdapter(activity, 0,arrayList)
            val listView = activity.findViewById(R.id.list_item) as ListView
            listView.adapter=adapter
        }

    }
}
