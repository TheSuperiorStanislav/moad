package com.skhlud.android.moad4

import android.support.v4.app.Fragment
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import android.view.View
import org.jetbrains.annotations.Nullable
import kotlinx.android.synthetic.main.fragment_home.*
import com.skhlud.android.moad4.ModelBuilder.Model


class HomeFragment : Fragment(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    @Nullable
    override fun onCreateView(inflater: LayoutInflater?, @Nullable container: ViewGroup?, @Nullable savedInstanceState: Bundle?): View? {
        val view = inflater!!.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        doTheThing.setOnClickListener {Model.DoTheThing(
                samplesize.text.toString().toInt(),
                mexp.text.toString().toDouble(),
                disp.text.toString().toDouble(),
                A1.text.toString().toDouble(),
                Fu1.text.toString(),
                A2.text.toString().toDouble(),
                Fu2.text.toString())
                result.text = "Altha1 = " + Model.altha1+" Altha2 = " + Model.altha2}
    }
}
