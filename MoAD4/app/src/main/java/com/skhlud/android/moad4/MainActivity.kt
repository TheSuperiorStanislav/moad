package com.skhlud.android.moad4

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    var home_fragment: Fragment = HomeFragment()
    var table_fragment: Fragment = TableFragment()
    var cur_fragment = 1
    var ft: FragmentTransaction = supportFragmentManager.beginTransaction()

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_home -> {
                cur_fragment = 1
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content, home_fragment)
                ft.commit()
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_table -> {
                cur_fragment = 2
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.content, table_fragment)
                ft.commit()
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)
        ft = supportFragmentManager.beginTransaction()
        when (cur_fragment) {
            1 -> ft.replace(R.id.content, home_fragment)
            2 -> ft.replace(R.id.content, table_fragment)
        }
        ft.commit()
    }

}
