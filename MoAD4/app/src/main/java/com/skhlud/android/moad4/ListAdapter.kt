package com.skhlud.android.moad4

import android.widget.ArrayAdapter
import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.row_layout.view.*


/**
 * Created by 1 on 16.05.2017.
 */

class ListAdapter
(getContext: Context, resource: Int, val items: ArrayList<ModelBuilder.Point>) : ArrayAdapter<ModelBuilder.Point>(getContext, resource, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
        val item = items[position]
        val inflater = context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.row_layout, null)
        view.i.text = view.i.text.toString() + (position+1)
        view.u1.text = view.u1.text.toString() + (Math.round(item.u1 * 100000.0) / 100000.0)
        view.u2.text = view.u2.text.toString() + (Math.round(item.u2 * 100000.0) / 100000.0)
        view.X.text = view.X.text.toString() + (Math.round(item.x * 100000.0) / 100000.0)
        view._X.text = view._X.text.toString() + (Math.round(item._x * 100000.0) / 100000.0)
        return view
    }

    override fun getCount(): Int {
        return items.size
    }

    override fun getItem(position: Int): ModelBuilder.Point {
        return this.items[position]
    }
}