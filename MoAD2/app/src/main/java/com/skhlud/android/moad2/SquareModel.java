package com.skhlud.android.moad2;

import java.util.Random;

/**
 * Created by 1 on 07.04.2017.
 */
public class SquareModel {//квадратичная модель
    private static SquareModel mySquareModel = new SquareModel();

    private Random rand = new Random();
    private Boolean objectType;//тип объекта
    private Double mExp, disp;//дисперисия и ожидание
    private Double u01, u02;// входы(не на что не влияют)
    private Double u1Delta, u2Delta;//дельта
    private Double n = 9.0;//кол-во точек
    private int s = 121;//кол-во измерений в точке
    //План
    private Double ExpMatrix[][] = new Double[][]{
            {1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0},//x0
            {1.0, -1.0, 1.0, -1.0, 1.0, -1.0, 0.0, 0.0, 0.0},//x1
            {1.0, 1.0, -1.0, -1.0, 0.0, 0.0, 1.0, -1.0, 0.0},//x2
            {1.0, -1.0, -1.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0},//x12
            {1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, -2.0 / 3.0, -2.0 / 3.0, -2.0 / 3.0},//x1'
            {1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, -2.0 / 3.0, -2.0 / 3.0, 1.0 / 3.0, 1.0 / 3.0, -2.0 / 3.0}};//x2'
    private Double a0Obj, a1Obj, a2Obj;
    private Double a11Obj, a12Obj, a22Obj;
    private Double a0Model, a1Model, a2Model;
    private Double a11Model, a12Model, a22Model;
    private Double b0_Model, b0Model, b1Model, b2Model;
    private Double b11Model, b12Model, b22Model;
    //массивы для дебага и расчетов
    private Double[][] objectOutput;
    private Double[] objectAverageOutput;
    private Double[] dispOutput;
    private Double dispAverageOutput;

    private Double[] modelAverageOutput;

    private Double Fisher;//фишер, duh
    private Double dispAdequacy;//дисперисия адекватности

    private Boolean isBetasOk;//Все ли коф. ок
    private Boolean isCochranPass;// прошел ли корчен(хз правильно написал)
    private Boolean[] isBetaPass;//Все ли коф. ок(для каждого отдельно)
    private Boolean isFisherPass;//Прошел ли фишер

    public static SquareModel getModel() {
        return mySquareModel;
    }

    private SquareModel() {
    }
    //При лин. объекте
    public void DoTheThing(Boolean objectType, Double u01, Double u02, Double u1Delta, Double u2Delta,
                           Double mExp, Double disp,
                           Double a0Obj, Double a1Obj, Double a2Obj) {
        this.objectType = objectType;
        this.u01 = u01;
        this.u02 = u02;
        this.u1Delta = u1Delta;
        this.u2Delta = u2Delta;
        this.mExp = mExp;
        this.disp = disp;
        this.a0Obj = a0Obj;
        this.a1Obj = a1Obj;
        this.a2Obj = a2Obj;
        DoAlgorithm();
    }
    //При квадратном объекте
    public void DoTheThing(Boolean objectType, Double u01, Double u02, Double u1Delta, Double u2Delta,
                           Double mExp, Double disp,
                           Double a0Obj, Double a1Obj, Double a2Obj,
                           Double a11Obj, Double a12Obj, Double a22Obj) {
        this.objectType = objectType;
        this.u01 = u01;
        this.u02 = u02;
        this.u1Delta = u1Delta;
        this.u2Delta = u2Delta;
        this.mExp = mExp;
        this.disp = disp;
        this.a0Obj = a0Obj;
        this.a1Obj = a1Obj;
        this.a2Obj = a2Obj;
        this.a11Obj = a11Obj;
        this.a12Obj = a12Obj;
        this.a22Obj = a22Obj;
        DoAlgorithm();
    }
    //Запуск алгортма
    private void DoAlgorithm() {
        isBetasOk = true;
        isBetaPass = new Boolean[6];
        isBetaPass[0] = true;
        isBetaPass[1] = true;
        isBetaPass[2] = true;
        isBetaPass[3] = true;
        isBetaPass[4] = true;
        isBetaPass[5] = true;
        FillObjectAverageOutput();
        FillDispOutput();
        CochranDistribution();
        FindAlptaBeta();
        StudentDistribution();
        if (!isBetasOk) {
            FindAlptaBeta();
            StudentDistribution();
        }
        FillModelAverageOutput();
        CalculateDispAdequacy();
        CalculateFisher();
    }
    //Измерения в точках, получаем выходы
    private void FillObjectAverageOutput() {
        Double temp = 0.0;
        objectOutput = new Double[9][s];
        objectAverageOutput = new Double[9];
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < s; i++) {
                if (objectType) {
                    objectOutput[j][i] = Func(ExpMatrix[0][j], ExpMatrix[1][j] * u1Delta, ExpMatrix[2][j] * u2Delta, ExpMatrix[1][j] * u1Delta, ExpMatrix[3][j] * u1Delta * u2Delta, ExpMatrix[2][j] * u2Delta);
                } else {
                    objectOutput[j][i] = Func(ExpMatrix[0][j], ExpMatrix[1][j] * u1Delta, ExpMatrix[2][j] * u2Delta);
                }
                temp += objectOutput[j][i];
            }
            objectAverageOutput[j] = temp / s;
            temp = 0.0;
        }
    }
    //Получаем дисперисии
    private void FillDispOutput() {
        Double temp = 0.0;
        dispOutput = new Double[9];
        for (int j = 0; j < 9; j++) {
            for (int i = 0; i < s; i++) {
                temp += Math.pow(objectOutput[j][i] - objectAverageOutput[j], 2);
            }
            dispOutput[j] = temp / ((double) s - 1.0);
            temp = 0.0;
        }
    }
    //Проверяем корчена
    private void CochranDistribution() {
        Double disMax = dispOutput[0];
        Double disSum = 0.0;
        for (int i = 0; i < 9; i++) {
            if (disMax < dispOutput[i])
                disMax = dispOutput[i];
            disSum += dispOutput[i];
        }
        if (disMax / disSum < 0.14) {
            isCochranPass = true;
        } else {
            isCochranPass = false;
        }
        dispAverageOutput = disSum / n;
    }
    //Находим кофициенты
    private void FindAlptaBeta() {
        b0Model = 0.0;
        b1Model = 0.0;
        b2Model = 0.0;
        b11Model = 0.0;
        b12Model = 0.0;
        b22Model = 0.0;

        for (int i = 0; i < 9; ++i) {
            b0Model += objectAverageOutput[i] * ExpMatrix[0][i];
            b1Model += objectAverageOutput[i] * ExpMatrix[1][i];
            b2Model += objectAverageOutput[i] * ExpMatrix[2][i];
            b11Model += objectAverageOutput[i] * ExpMatrix[4][i];
            b12Model += objectAverageOutput[i] * ExpMatrix[3][i];
            b22Model += objectAverageOutput[i] * ExpMatrix[5][i];
        }

        if (isBetaPass[1]) {
            b1Model /= 6.0;

        } else {
            b1Model = 0.0;
        }
        if (isBetaPass[2]) {
            b2Model /= 6.0;

        } else {
            b2Model = 0.0;
        }
        if (isBetaPass[3]) {
            b11Model /= 2.0;

        } else {
            b11Model = 0.0;
        }
        if (isBetaPass[4]) {
            b12Model /= 4.0;

        } else {
            b12Model = 0.0;
        }
        if (isBetaPass[5]) {
            b22Model /= 2.0;

        } else {
            b22Model = 0.0;
        }
        if (isBetaPass[0]) {
            b0Model /= n;
            b0_Model = b0Model;
            b0Model += -b11Model * (2.0 / 3.0) - b22Model * (2.0 / 3.0);
        } else {
            b0Model = 0.0;
        }

        a0Model = b0Model;
        a1Model = b1Model / u1Delta;
        a2Model = b2Model / u2Delta;

        a11Model = b11Model / Math.pow(u1Delta, 2);

        a12Model = b12Model / (u1Delta * u2Delta);

        a22Model = b22Model / Math.pow(u2Delta, 2);
    }
    //Проверяем значимость
    private void StudentDistribution() {
        Double temp;
        isBetaPass = new Boolean[6];
        temp = b0Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[0] = false;
            isBetasOk = false;
        } else {
            isBetaPass[0] = true;
        }

        temp = b1Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[1] = false;
            isBetasOk = false;
        } else {
            isBetaPass[1] = true;
        }

        temp = b2Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[2] = false;
            isBetasOk = false;
        } else {
            isBetaPass[2] = true;
        }
        temp = b11Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[3] = false;
            isBetasOk = false;
        } else {
            isBetaPass[3] = true;
        }

        temp = b12Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[4] = false;
            isBetasOk = false;
        } else {
            isBetaPass[4] = true;
        }

        temp = b22Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[5] = false;
            isBetasOk = false;
        } else {
            isBetaPass[5] = true;
        }
    }
    //Строим модель
    private void FillModelAverageOutput() {
        modelAverageOutput = new Double[9];
        for (int j = 0; j < 9; j++) {
            modelAverageOutput[j] = ModelFunc(ExpMatrix[0][j], ExpMatrix[1][j], ExpMatrix[2][j], ExpMatrix[4][j], ExpMatrix[3][j], ExpMatrix[5][j]);
        }
    }
    //Получаем дисперисию адекватности
    private void CalculateDispAdequacy() {
        Double temp = 0.0;
        for (int j = 0; j < 9; j++) {
            temp += Math.pow(objectAverageOutput[j] - modelAverageOutput[j], 2);

        }
        dispAdequacy = temp / (n - 2.0 - 1.0);
    }
    //Считаем фишера
    private void CalculateFisher() {
        Fisher = n * dispAdequacy / dispAverageOutput;
        if (Fisher < 3.70) {
            isFisherPass = true;
        } else {
            isFisherPass = false;
        }
    }

    private Double MethodOfPolarCoordinates() {
        Double U1, V1, U2, V2, S, X1, X2;
        Double num;
        do {
            U1 = rand.nextDouble();
            U2 = rand.nextDouble();
            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        } while (S >= 1);
        X1 = V1 * Math.sqrt(-2 * Math.log(S) / S);
        //X2 = V2 * Math.sqrt(-2 * Math.log(S) / S);
        num = Math.sqrt(disp) * X1 + mExp;
        return num;
    }
    //Помеха
    private Double CentralLimitTheorem() {
        return Math.sqrt(disp) * Math.sqrt(12.0 / 100) * CalcForCLT() + mExp;
    }

    private Double CalcForCLT() {

        Double Result = 0.0;
        for (int i = 0; i < 100; i++) {
            Result += (rand.nextDouble() - 0.5);
        }
        return Result;
    }
    //Функция
    private Double Func(Double u0, Double u1, Double u2) {
        return a0Obj * u0 + a1Obj * u1 + a2Obj * u2 + CentralLimitTheorem();
    }
    //Модель
    private Double Func(Double u0, Double u1, Double u2, Double u11, Double u12, Double u22) {
        return a0Obj * u0 + a1Obj * u1 + a2Obj * u2 + a11Obj * Math.pow(u11, 2) + a12Obj * u12 + a22Obj * Math.pow(u22, 2) + CentralLimitTheorem();
    }
    //
    private Double ModelFunc(Double x0, Double x1, Double x2, Double x11, Double x12, Double x22) {
        return b0_Model * x0 + b1Model * x1 + b2Model * x2 + b11Model * x11 + b12Model * x12 + b22Model * x22;
    }
    //Получение результатов
    public String[] GetTestsResults() {
        String str = "";
        if (objectType) {
            str += "Объект:\n y = " + a0Obj + " + " + a1Obj + "*(u1 - " + u01 + ") + " + a2Obj + "*(u2 - " + u02 + ") + " +
                    a11Obj + "*(u1 - " + u01 + ")^2 + " + a12Obj + "*(u1 - " + u01 + ")*(u2 - " + u02 + ") + " + a22Obj + "(u2 - " + u02 + ")^2 +" + "N(m,D)" + "#";
        } else {
            str += "Объект:\n y = " + a0Obj + " + " + a1Obj + "*(u1-" + u01 + ") + " + a2Obj + "*(u2-" + u02 + ") + N(m,D)" + "#";
        }
        str += "Модель:\n y = " + Math.round(a0Model * 100000d) / 100000d + " + " + Math.round(a1Model * 100000d) / 100000d + "*(u1 - " + u01 + ") + " + Math.round(a2Model * 100000d) / 100000d + "*(u2 - " + u02 + ") + " +
                Math.round(a11Model * 100000d) / 100000d + "*(u1 - " + u01 + ")^2 + " + Math.round(a12Model * 100000d) / 100000d + "*(u1 - " + u01 + ")*(u2 - " + u02 + ") + " + Math.round(a22Model * 100000d) / 100000d + "(u2 - " + u02 + ")^2 " + "#";
        str += Math.round(a0Model * 100000d) / 100000d + "#" + Math.round(a1Model * 100000d) / 100000d + "#" + Math.round(a2Model * 100000d) / 100000d + "#"
                + Math.round(a11Model * 100000d) / 100000d + "#" + Math.round(a12Model * 100000d) / 100000d + "#" + Math.round(a22Model * 100000d) / 100000d + "#"
                + Math.round(b0Model * 100000d) / 100000d + "#" + Math.round(b1Model * 100000d) / 100000d + "#" + Math.round(b2Model * 100000d) / 100000d + "#"
                + Math.round(b11Model * 100000d) / 100000d + "#" + Math.round(b12Model * 100000d) / 100000d + "#" + Math.round(b22Model * 100000d) / 100000d + "#"
                + Math.round(dispAverageOutput * 100000d) / 100000d + "#" + Math.round(dispAdequacy * 100000d) / 100000d + "#" + Math.round(Fisher * 100000d) / 100000d + "#"
                + isCochranPass + "#" + isFisherPass + "#" + isBetaPass[0] + "#" + isBetaPass[1] + "#" + isBetaPass[2] + "#" + isBetaPass[3] + "#" + isBetaPass[4] + "#" + isBetaPass[5];
        String[] Data = str.split("#");
        return Data;
    }
}
