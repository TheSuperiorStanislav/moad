package com.skhlud.android.moad2;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void DoTheThing(View view) {
        try {
            TextView Result = (TextView) findViewById(R.id.Result);
            Result.setText("");
            Double editAltha0 = Double.parseDouble(((EditText) findViewById(R.id.editAltha0)).getText().toString());
            Double editAltha1 = Double.parseDouble(((EditText) findViewById(R.id.editAltha1)).getText().toString());
            Double editAltha2 = Double.parseDouble(((EditText) findViewById(R.id.editAltha2)).getText().toString());
            Double editMexp = Double.parseDouble(((EditText) findViewById(R.id.editMexp)).getText().toString());
            Double editDisp = Double.parseDouble(((EditText) findViewById(R.id.editDisp)).getText().toString());
            Double editU1 = Double.parseDouble(((EditText) findViewById(R.id.editU1)).getText().toString());
            Double editU2 = Double.parseDouble(((EditText) findViewById(R.id.editU2)).getText().toString());
            Double editU1Delta = Double.parseDouble(((EditText) findViewById(R.id.editU1Delta)).getText().toString());
            Double editU2Delta = Double.parseDouble(((EditText) findViewById(R.id.editU2Delta)).getText().toString());
            Boolean objectType = ((Switch) findViewById(R.id.ObjectType)).isChecked();
            if (objectType) {
                Double editAltha11 = Double.parseDouble(((EditText) findViewById(R.id.editAltha11)).getText().toString());
                Double editAltha12 = Double.parseDouble(((EditText) findViewById(R.id.editAltha12)).getText().toString());
                Double editAltha22 = Double.parseDouble(((EditText) findViewById(R.id.editAltha22)).getText().toString());
                LinearModel.getModel().DoTheThing(objectType, editU1, editU2, editU1Delta, editU2Delta, editMexp, editDisp, editAltha0, editAltha1, editAltha2, editAltha11, editAltha12, editAltha22);
                SquareModel.getModel().DoTheThing(objectType, editU1, editU2, editU1Delta, editU2Delta, editMexp, editDisp, editAltha0, editAltha1, editAltha2, editAltha11, editAltha12, editAltha22);
            } else {
                LinearModel.getModel().DoTheThing(objectType, editU1, editU2, editU1Delta, editU2Delta, editMexp, editDisp, editAltha0, editAltha1, editAltha2);
                SquareModel.getModel().DoTheThing(objectType, editU1, editU2, editU1Delta, editU2Delta, editMexp, editDisp, editAltha0, editAltha1, editAltha2);
            }
            String[] ResultsLinear = LinearModel.getModel().GetTestsResults();
            String[] ResultsSquare = SquareModel.getModel().GetTestsResults();
            String str = "";
            str += "Линейная модель\n";
            str += ResultsLinear[0] + "\n";
            str += ResultsLinear[1] + "\n";
            str += "a0 = " + ResultsLinear[2] + ", a1 = " + ResultsLinear[3] + ", a2 = " + ResultsLinear[4] + "\n";
            str += "b0 = " + ResultsLinear[5] + ", b1 = " + ResultsLinear[6] + ", b2 = " + ResultsLinear[7] + "\n";
            str += "dispAverOut = " + ResultsLinear[8] + ", dispAdequacy = " + ResultsLinear[9] + "\n";
            str += "isCochranPass = " + ResultsLinear[11] + ", Fisher = " + ResultsLinear[10] + ", isFisherPass = " + ResultsLinear[15] + "\n";
            str += "isBetaPass0 = " + ResultsLinear[12] + ", isBetaPass1 = " + ResultsLinear[13] + ", isBetaPass2 = " + ResultsLinear[14] + "\n";
            str += "Квадратичная модель\n";
            str += ResultsSquare[0] + "\n";
            str += ResultsSquare[1] + "\n";
            str += "a0 = " + ResultsSquare[2] + ", a1 = " + ResultsSquare[3] + ", a2 = " + ResultsSquare[4] + "\n";
            str += "a11 = " + ResultsSquare[5] + ", a12 = " + ResultsSquare[6] + ", a22 = " + ResultsSquare[7] + "\n";
            str += "b0 = " + ResultsSquare[8] + ", b1 = " + ResultsSquare[9] + ", b2 = " + ResultsSquare[10] + "\n";
            str += "b11 = " + ResultsSquare[11] + ", b12 = " + ResultsSquare[12] + ", b22 = " + ResultsSquare[13] + "\n";
            str += "dispAverOut = " + ResultsSquare[14] + ", dispAdequacy = " + ResultsSquare[15] + "\n";
            str += "isCochranPass = " + ResultsSquare[17] + ", Fisher = " + ResultsSquare[16] + ", isFisherPass = " + ResultsSquare[18] + "\n";
            str += "isBetaPass0 = " + ResultsSquare[19] + ", isBetaPass1 = " + ResultsSquare[20] + ", isBetaPass2 = " + ResultsSquare[21] + "\n";
            str += "isBetaPass11 = " + ResultsSquare[22] + ", isBetaPass12 = " + ResultsSquare[23] + ", isBetaPass22 = " + ResultsSquare[24] + "\n";
            Result.setText(str);
        } catch (Exception ex) {
            Toast.makeText(MainActivity.this, ex.getLocalizedMessage(),
                    Toast.LENGTH_SHORT).show();
        }
    }
}
