package com.skhlud.android.moad2;
import java.util.Random;

/**
 * Created by 1 on 05.04.2017.
 */

public class LinearModel {//линейная модель
    private static LinearModel myLinearModel = new LinearModel();

    private Random rand = new Random();
    private Boolean objectType;//тип объекта
    private Double mExp, disp;//дисперисия и ожидание
    private Double u01, u02;// входы(не на что не влияют)
    private Double u1Delta, u2Delta;//дельта
    private Double n = 4.0;//кол-во точек
    private int s = 121;//кол-во измерений в точке
    //план
    private Double ExpMatrix[][] = new Double[][]{{1.0, 1.0, 1.0, 1.0},//x0
                                                  {1.0, -1.0, 1.0, -1.0},//x1
                                                  {1.0, 1.0, -1.0, -1.0}};//x2
    private Double a0Obj, a1Obj, a2Obj;//
    private Double a11Obj, a12Obj, a22Obj;//
    private Double a0Model, a1Model, a2Model;//
    private Double b0Model, b1Model, b2Model;//
    //массивы для дебага и расчетов
    private Double[][] objectOutput;
    private Double[] objectAverageOutput;
    private Double[] dispOutput;
    private Double dispAverageOutput;

    private Double[] modelAverageOutput;

    private Double Fisher;//фишер, duh
    private Double dispAdequacy;//дисперисия адекватности

    private Boolean isBetasOk;//Все ли коф. ок
    private Boolean isCochranPass;// прошел ли корчен(хз правильно написал)
    private Boolean[] isBetaPass;//Все ли коф. ок(для каждого отдельно)
    private Boolean isFisherPass;//Прошел ли фишер

    public static LinearModel getModel() {
        return myLinearModel;
    }

    private LinearModel() {
    }
    //При лин. объекте
    public void DoTheThing(Boolean objectType,Double u01, Double u02, Double u1Delta, Double u2Delta,
                           Double mExp, Double disp,
                           Double a0Obj, Double a1Obj, Double a2Obj) {
        this.objectType = objectType;
        this.u01 = u01;
        this.u02 = u02;
        this.u1Delta = u1Delta;
        this.u2Delta = u2Delta;
        this.mExp = mExp;
        this.disp = disp;
        this.a0Obj = a0Obj;
        this.a1Obj = a1Obj;
        this.a2Obj = a2Obj;
        DoAlgorithm();
    }
    //При квадратном объекте
    public void DoTheThing(Boolean objectType,Double u01, Double u02, Double u1Delta, Double u2Delta,
                           Double mExp, Double disp,
                           Double a0Obj, Double a1Obj, Double a2Obj,
                           Double a11Obj, Double a12Obj, Double a22Obj) {
        this.objectType = objectType;
        this.u01 = u01;
        this.u02 = u02;
        this.u1Delta = u1Delta;
        this.u2Delta = u2Delta;
        this.mExp = mExp;
        this.disp = disp;
        this.a0Obj = a0Obj;
        this.a1Obj = a1Obj;
        this.a2Obj = a2Obj;
        this.a11Obj = a11Obj;
        this.a12Obj = a12Obj;
        this.a22Obj = a22Obj;
        DoAlgorithm();
    }
    //Запуск алгортма
    private void DoAlgorithm() {
        isBetasOk = true;
        isBetaPass = new Boolean[3];
        isBetaPass[0] = true;
        isBetaPass[1] = true;
        isBetaPass[2] = true;
        FillObjectAverageOutput();
        FillDispOutput();
        CochranDistribution();
        FindAlptaBeta();
        StudentDistribution();
        if (!isBetasOk)
        {
            FindAlptaBeta();
            StudentDistribution();
        }
        FillModelAverageOutput();
        CalculateDispAdequacy();
        CalculateFisher();
    }
    //Измерения в точках, получаем выходы
    private void FillObjectAverageOutput() {
        Double temp = 0.0;
        objectOutput = new Double[4][s];
        objectAverageOutput = new Double[4];
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < s; i++) {
                objectOutput[j][i] = Func(ExpMatrix[0][j],ExpMatrix[1][j] * u1Delta,ExpMatrix[2][j] * u2Delta);
                temp += objectOutput[j][i];
            }
            objectAverageOutput[j] = temp / s;
            temp = 0.0;
        }
    }
    //Получаем дисперисии
    private void FillDispOutput() {
        Double temp = 0.0;
        dispOutput = new Double[4];
        for (int j = 0; j < 4; j++) {
            for (int i = 0; i < s; i++) {
                temp += Math.pow(objectOutput[j][i] - objectAverageOutput[j], 2);
            }
            dispOutput[j] = temp / (s - 1.0);
            temp = 0.0;
        }
    }
    //Проверяем корчена
    private void CochranDistribution() {
        //TODO: get more info
        Double disMax = dispOutput[0];
        Double disSum = 0.0;
        for (int i = 0; i < 4; i++) {
            if (disMax < dispOutput[i])
                disMax = dispOutput[i];
            disSum += dispOutput[i];
        }
        if (disMax / disSum < 0.4031) {
            isCochranPass = true;
        } else {
            isCochranPass = false;
        }
        dispAverageOutput = disSum / n;
    }
    //Находим кофициенты
    private void FindAlptaBeta() {
        b0Model = 0.0;
        b1Model = 0.0;
        b2Model = 0.0;

        for (int i = 0; i < 4; ++i) {
            b0Model += objectAverageOutput[i] * ExpMatrix[0][i];
            b1Model += objectAverageOutput[i] * ExpMatrix[1][i];
            b2Model += objectAverageOutput[i] * ExpMatrix[2][i];
        }

        if(isBetaPass[0])
        {
            b0Model /= n;

        }
        else {
            b0Model = 0.0;
        }
        if(isBetaPass[1])
        {
            b1Model /= n;

        }
        else {
            b1Model = 0.0;
        }
        if(isBetaPass[2])
        {
            b2Model /= n;

        }
        else {
            b2Model = 0.0;
        }

        a0Model = b0Model;

        a1Model = b1Model / u1Delta;

        a2Model = b2Model / u2Delta;
    }
    //Проверяем значимость
    private void StudentDistribution() {
        Double temp;
        isBetaPass = new Boolean[3];
        temp = b0Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[0] = false;
            isBetasOk = false;
        } else {
            isBetaPass[0] = true;
        }

        temp = b1Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[1] = false;
            isBetasOk = false;
        } else {
            isBetaPass[1] = true;
        }

        temp = b2Model / (Math.sqrt(dispAverageOutput) / Math.sqrt(n * s));
        if (temp < 1.96 && temp > -1.96) {
            isBetaPass[2] = false;
            isBetasOk = false;
        } else {
            isBetaPass[2] = true;
        }
    }
    //Строим модель
    private void FillModelAverageOutput() {
        modelAverageOutput = new Double[4];
        for (int j = 0; j < 4; j++) {
            modelAverageOutput[j] = ModelFunc(ExpMatrix[0][j], ExpMatrix[1][j], ExpMatrix[2][j]);
        }
    }
    //Получаем дисперисию адекватности
    private void CalculateDispAdequacy() {
        Double temp = 0.0;
        for (int j = 0; j < 4; j++) {
            temp += Math.pow(objectAverageOutput[j] - modelAverageOutput[j], 2);

        }
        dispAdequacy = temp / (n - 2.0 - 1.0);
    }
    //Считаем фишера
    private void CalculateFisher() {
        Fisher = n * dispAdequacy / dispAverageOutput;
        //не уверен в этом моменте(в значении)
        if (Fisher < 8.55) {
            isFisherPass = true;
        } else {
            isFisherPass = false;
        }
    }

    private Double MethodOfPolarCoordinates() {
        Double U1, V1, U2, V2, S, X1, X2;
        Double num;
        do {
            U1 = rand.nextDouble();
            U2 = rand.nextDouble();
            V1 = 2 * U1 - 1;
            V2 = 2 * U2 - 1;
            S = V1 * V1 + V2 * V2;
        } while (S >= 1);
        X1 = V1 * Math.sqrt(-2 * Math.log(S) / S);
        //X2 = V2 * Math.sqrt(-2 * Math.log(S) / S);
        num = Math.sqrt(disp) * X1 + mExp;
        return num;
    }
    //Помеха
    private Double  CentralLimitTheorem()
    {
        return Math.sqrt(disp) * Math.sqrt(12.0 / 100) * CalcForCLT() + mExp;
    }
    private Double CalcForCLT()
    {

        Double Result = 0.0;
        for (int i = 0; i < 100; i++)
        {
            Result += (rand.nextDouble() - 0.5);
        }
        return Result;
    }
    //Функция
    private Double Func(Double u0, Double u1, Double u2) {
        if (objectType) {
            return a0Obj * u0 + a1Obj * u1 + a2Obj * u2 + a11Obj * u1 * u1 + a12Obj * u1 * u2 + a22Obj * u2 * u2 + CentralLimitTheorem();
        }
        else {
            return a0Obj * u0 + a1Obj * u1 + a2Obj * u2 + CentralLimitTheorem();
        }
    }
    //Модель
    private Double ModelFunc(Double x0, Double x1, Double x2) {
        return b0Model * x0 + b1Model * x1 + b2Model * x2;
    }
    //Получение результатов
    public String[] GetTestsResults() {
        String str = "";
        if (objectType)
        {
            str += "Объект:\n y = " +a0Obj + " + " +a1Obj + "*(u1 - " + u01 + ") + " +a2Obj + "*(u2 - " + u02 + ") + "+
                    a11Obj +"*(u1 - " + u01 + ")^2 + " + a12Obj +"*(u1 - " + u01 + ")*(u2 - "+u02+") + "+ a22Obj +"(u2 - " + u02 + ")^2 +" +"N(m,D)"+"#";
        }
        else
        {
            str += "Объект:\n y = " +a0Obj + " + " +a1Obj + "*(u1-" + u01 + ") + " +a2Obj + "*(u2-" + u02 + ") + N(m,D)"+"#";
        }
        str += "Модель:\n y = " +Math.round(a0Model * 100000d) / 100000d + " + " +Math.round(a1Model * 100000d) / 100000d + "*(u1 - " + u01 + ") + " +Math.round(a2Model * 100000d) / 100000d + "*(u2 - " + u02 + ")"+"#";
        str +=    Math.round(a0Model * 100000d) / 100000d + "#" + Math.round(a1Model * 100000d) / 100000d + "#" + Math.round(a2Model * 100000d) / 100000d + "#"
                + Math.round(b0Model * 100000d) / 100000d + "#" + Math.round(b1Model * 100000d) / 100000d + "#" + Math.round(b2Model * 100000d) / 100000d + "#"
                + Math.round(dispAverageOutput * 100000d) / 100000d + "#" + Math.round(dispAdequacy * 100000d) / 100000d + "#" + Math.round(Fisher * 100000d) / 100000d + "#"
                + isCochranPass + "#" + isBetaPass[0] + "#" + isBetaPass[1] + "#" + isBetaPass[2] + "#" + isFisherPass;
        String[] Data = str.split("#");
        return Data;
    }

}
