package com.skhlud.android.moad3;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    static Fragment home_fragment;
    static Fragment funcgrath_fragment;
    static Fragment bettas_fragment;
    static Fragment second_funcgrath_fragment;
    static int cur_fragment = 1;
    FragmentTransaction ft;

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            try {
                ft = getSupportFragmentManager().beginTransaction();
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        cur_fragment = 1;
                        ft.replace(R.id.content, home_fragment);
                        ft.commit();
                        return true;
                    case R.id.navigation_graph1:
                        cur_fragment = 2;
                        ft.replace(R.id.content, funcgrath_fragment);
                        ft.commit();
                        return true;
                    case R.id.navigation_graph2:
                        cur_fragment = 3;
                        ft.replace(R.id.content, second_funcgrath_fragment);
                        ft.commit();
                        return true;
                    case R.id.navigation_table:
                        cur_fragment = 4;
                        ft.replace(R.id.content, bettas_fragment);
                        ft.commit();
                        return true;
                }
            } catch (Exception ex) {
                Toast toast = Toast.makeText(getApplicationContext(), ex.getMessage() + "\n" + ex.getLocalizedMessage(), Toast.LENGTH_SHORT);
                toast.show();
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        try {
            if (home_fragment == null)
                home_fragment = new HomeFragment();
            if (funcgrath_fragment == null)
                funcgrath_fragment = new FuncGrathFragment();
            if (second_funcgrath_fragment == null)
                second_funcgrath_fragment = new SecondFuncGrathFragment();
            if (bettas_fragment == null)
                bettas_fragment = new BettasFragment();
            ft = getSupportFragmentManager().beginTransaction();
            switch (cur_fragment) {
                case 1:
                    ft.replace(R.id.content, home_fragment);
                    break;
                case 2:
                    ft.replace(R.id.content, funcgrath_fragment);
                    break;
                case 3:
                    ft.replace(R.id.content, second_funcgrath_fragment);
                    break;
                case 4:
                    ft.replace(R.id.content, bettas_fragment);
                    break;
            }
            ft.commit();
        } catch (Exception ex) {
            Toast toast = Toast.makeText(getApplicationContext(), ex.getMessage() + "\n" + ex.getLocalizedMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }

    }
}
