package com.skhlud.android.moad3;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;

import java.util.ArrayList;
import java.util.Random;


/**
 * Created by 1 on 07.05.2017.
 */

public class ModelBuilder {
    private static Random random;
    static int n; // размер выборки
    static double staringDot; // начальная точка
    static double delta; // дельта или шаг
    private static double mExp; // мат. ожидание (оно посути не нужно но у меня есть)
    private static double disp; // дисперсия
    private static int coreType; // тип ядра
    private static String function = ""; //функция
    private static int b_or_h; //хотел сделать и поиск по h но чет не оч
    private static int seacrhMethod; //выбраный метод

    private static HomeFragment.MyTask task; //не обращай внимания

    static double betaorH_Optimal = -1.0; //оптимальное
    private static boolean isBetaorH_Found = false; //если найдено оптим. (см. скользящий экз.)

    static double [][] dots; //объект
    static double [][] dotsWithInterference; // объект с помехой
    static double [][] dotsModel; // модель
    static ArrayList<double[]> betasI; // данные для таблицы
    static boolean isDone = false; // для таска

    static void DotheThing(int n, double staringDot, double delta, double mExp, double disp,
                           int coreType,int b_or_h,int seacrhMethod,
                           String function,HomeFragment.MyTask task) {
        isDone = false;
        isBetaorH_Found = false;
        random = new Random();
        ModelBuilder.n=n;
        ModelBuilder.staringDot = staringDot;
        ModelBuilder.delta = delta;
        ModelBuilder.mExp = mExp;
        ModelBuilder.disp = disp;
        ModelBuilder.coreType = coreType+1;
        ModelBuilder.b_or_h = b_or_h+1;
        ModelBuilder.seacrhMethod = seacrhMethod+1;
        ModelBuilder.function = function;
        ModelBuilder.task = task;
        dots = new double[2][n];
        dotsWithInterference = new double[2][n];
        dotsModel = new double[2][n];
        betasI = new ArrayList<>();
        DoAlgorithm();
        isDone = true;
    }
    //Запуск алгоритма
    private static void DoAlgorithm() {
        CalculateDotsObject();
        FindOptimalBeta();
        CalculateDotsModel();
    }
    //Расчет точек с помехой и без
    private static void CalculateDotsObject() {
        for(int i = 0;i<n;i++)
        {
            task.onProgressUpdate(1);
            dots[0][i]=staringDot + (double) i * delta;
            dots[1][i]=Func(dots[0][i],false);
            dotsWithInterference[0][i]=staringDot + (double) i * delta;
            dotsWithInterference[1][i]=Func(dotsWithInterference[0][i],true);

        }
    }
    //Поиск оптимального Бета
    private static void FindOptimalBeta() {
        switch (seacrhMethod) {
            case 1:
                betaorH_Optimal = FibonacciMethod();
                break;
            case 2:
                betaorH_Optimal = FindOptimal();
                break;
        }
    }
    //Расчет точек для модели
    private static void CalculateDotsModel() {
        for (int i = 0; i < n; i++) {
            task.onProgressUpdate(1);
            dotsModel[0][i]=dotsWithInterference[0][i];
            dotsModel[1][i]=CalculateRegresion(betaorH_Optimal,dotsWithInterference[0][i],i);
        }
    }
    //Помеха
    private static double  CentralLimitTheorem() {
        return Math.sqrt(disp) * Math.sqrt(12.0 / 100) * CalcForCLT() + mExp;
    }
    private static double CalcForCLT() {

        Double Result = 0.0;
        for (int i = 0; i < 100; i++)
        {
            Result += (random.nextDouble() - 0.5);
        }
        return Result;
    }
    //Ядра
    private static double Cores(double z) {
        double K = 0.0;
        if (Math.abs(z) <= 1) {
            switch (coreType) {
                case 1:
                    K = 0.5;
                    break;
                case 2:
                    K = 1.0 - Math.abs(z);
                    break;
                case 3:
                    K = 0.75 * (1.0 - Math.pow(z, 2));
                    break;
                case 4:
                    K = (1.0 + 2.0 * Math.abs(z)) * Math.pow(1.0 - Math.abs(z), 2);
                    break;
            }
        }
        return K;
    }
    //Функция
    private static double Func(double x,boolean mode) {
        Expression exp = new ExpressionBuilder(function)
                .variables("x")
                .build()
                .setVariable("x", x);
        if (mode)
        {
            return exp.evaluate() + CentralLimitTheorem();
        }
        else
        {
            return exp.evaluate();
        }
    }
    //Расчет для ядра
    private static double CalculateCore(double betaorH,double  x,int index) {
        double result =0.0;
        switch (b_or_h) {
            case 1:
                result = betaorH * (x - dotsWithInterference[0][index]) / delta;
                break;
            case 2:
                result = (x - dotsWithInterference[0][index]) / betaorH;
                break;
        }
        return result;
    }
    //Расчет оценки регресиии
    private static double CalculateRegresion(double betaorH,double x,int index) {
        double temp1 = 0;
        double temp2 = 0;
        for (int i = 0; i < n; i++) {
            if (i != index || isBetaorH_Found)//для скользящего экзамена(смотри в полной методичке, ну или если знаешь как коротко и понятно написать пиши)
                temp1 += Cores(CalculateCore(betaorH,x,i));
        }
        for (int i = 0; i < n; i++) {
            if (i != index || isBetaorH_Found)//для скользящего экзамена(смотри в полной методичке, ну или если знаешь как коротко и понятно написать пиши)
                temp2 += dotsWithInterference[1][i] * Cores(CalculateCore(betaorH,x,i));
        }
        return temp2 / temp1;
    }
    //Расчет критерия для проверки
    private static double CalculateI(double betaorH) {
        double I;
        double temp=0;
        for (int i = 0; i < n; i++) {
            double Ns = CalculateRegresion(betaorH, dotsWithInterference[0][i], i);
            temp += Math.pow(dotsWithInterference[1][i] - Ns, 2);
        }
        I = temp / n;
        return I;
    }
    //Метод Фибоначи
    private static double FibonacciMethod() {
        betasI.clear();
        double a = 0.0, b = 1.0;
        if (b_or_h == 2) {
            a = 0.0;
            b = 100.0;
        }
        double l = 0.01;
        int k = 0;
        int N = 2;
        double L = Math.abs(a - b);
        double[] fibNums;
        int progress;
        do {
            N++;
            fibNums = new double[N];
            fibNums[0] = 1;
            fibNums[1] = 1;
            for (int i = 2; i < N; i++) {
                fibNums[i] = fibNums[i - 1] + fibNums[i - 2];
            }
        } while (fibNums[N - 1] < L / l);
        progress = 100 / N;
        double y0 = a + (fibNums[N - 3] / fibNums[N - 1]) * (b - a);
        double z0 = a + (fibNums[N - 2] / fibNums[N - 1]) * (b - a);
        Boolean stillSearching = true;
        while (stillSearching) {
            double fy = CalculateI(y0);
            double fz = CalculateI(z0);
            if (fy <= fz) {
                b = z0;
                z0 = y0;
                y0 = a + (fibNums[N - k - 4] / fibNums[N - k - 2]) * (b - a);
            } else {
                a = y0;
                y0 = z0;
                z0 = a + (fibNums[N - k - 3] / fibNums[N - k - 2]) * (b - a);
            }
            if (k != N - 4) {
                k++;
                task.onProgressUpdate(progress);
                betasI.add(new double[]{(a + b) / 2.0, CalculateI((a + b) / 2.0)});
            } else {
                k++;
                stillSearching = false;
                y0 = (a + b) / 2.0;
                z0 = y0 + 0.01;
                fy = CalculateI(y0);
                fz = CalculateI(z0);
                if (fy <= fz) {
                    b = z0;
                } else {
                    a = y0;
                }
                betasI.add(new double[]{(a + b) / 2.0, CalculateI((a + b) / 2.0)});
            }
        }
        return (a + b) / 2.0;
    }
    //Подбор
    private static double FindOptimal() {
        isBetaorH_Found = false;
        betasI.clear();
        double Bmin=0.01;
        double Imin=Double.MAX_VALUE;
        double B=0.01;
        double step = 0.01;
        if (b_or_h == 2) {
            B=1;
            step = 1;
        }
        for (int i = 0;i<100;i++)
        {
            task.onProgressUpdate(1);
            B = Math.round(B * 100000d) / 100000d;
            double temp = CalculateI(B);
            betasI.add(new double []{B,CalculateI(B)});
            if (temp < Imin)
            {
                Bmin = B;
                Imin = temp;
            }
            B+=step;
        }
        isBetaorH_Found = true;
        return Math.round(Bmin * 100000d) / 100000d;
    }
}
