package com.skhlud.android.moad3;

import android.app.Activity;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.PointsGraphSeries;


public class SecondFuncGrathFragment extends Fragment {
    Activity mainactivity;
    GraphView graph;
    double data1[][];
    double data2[][];
    static LineGraphSeries<DataPoint> series1;
    static LineGraphSeries<DataPoint> series2;
    static MyTask task;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        if (ModelBuilder.isDone) {
            view = inflater.inflate(R.layout.fragment_func_grath, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment_oh_no, container, false);
        }
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (task == null) {
            task = new MyTask();
        }
        mainactivity = getActivity();
        if (ModelBuilder.isDone) {
            graph = (GraphView) mainactivity.findViewById(R.id.Graph);
            if (task.getStatus() == AsyncTask.Status.PENDING || task.getStatus() == AsyncTask.Status.FINISHED) {
                task = new MyTask();
                task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            } else if (task.getStatus() == AsyncTask.Status.RUNNING) {
                ReloadGraph(false);
            }
        }
    }

    void ReloadGraph(boolean flag)
    {
        if (flag) {
            series1 = new LineGraphSeries<>();
            series2 = new LineGraphSeries<>();
            series1.setColor(Color.RED);
            series1.setTitle("Обьект");
            series2.setThickness(5);
            series2.setColor(Color.BLUE);
            series2.setThickness(5);
            series2.setTitle("Модель");
            graph.getViewport().setMinX(ModelBuilder.staringDot);
            graph.getViewport().setMaxX(ModelBuilder.staringDot + ModelBuilder.n * ModelBuilder.delta);
        }
        graph.removeAllSeries();
        graph.addSeries(series1);
        graph.addSeries(series2);
        graph.getViewport().setScalable(true);
        graph.getViewport().setScrollable(true);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.setTitle("График модели и обьекта без помехи");
    }

    class MyTask extends AsyncTask<Integer, Integer, String> {
        DataPoint dataPoint1;
        DataPoint dataPoint2;
        int size = ModelBuilder.n;
        double step = ModelBuilder.delta;
        @Override
        protected String doInBackground(Integer... params) {
            for (int i = 0; i < size; i++) {
                if (!isCancelled()) {
                    SystemClock.sleep(50);
                    dataPoint1 = new DataPoint(data1[0][i], data1[1][i]);
                    dataPoint2 = new DataPoint(data2[0][i], data2[1][i]);
                    publishProgress(i + 1);
                } else {
                    break;
                }
            }
            return "Task Completed.";
        }

        @Override
        protected void onPreExecute() {
            data1 = ModelBuilder.dots;
            data2 = ModelBuilder.dotsModel;
            ReloadGraph(true);
        }

        @Override
        protected void onPostExecute(String result) {
            //graph.getViewport().setXAxisBoundsManual(true);
            graph.getLegendRenderer().setVisible(true);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            series1.appendData(dataPoint1, true, values[0]);
            series2.appendData(dataPoint2, true, values[0]);
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setMinX(ModelBuilder.staringDot-step);
            graph.getViewport().setMaxX(step+ModelBuilder.staringDot + ModelBuilder.n * ModelBuilder.delta);
            if (isCancelled())
            {
                ReloadGraph(true);
            }
        }

    }
}
