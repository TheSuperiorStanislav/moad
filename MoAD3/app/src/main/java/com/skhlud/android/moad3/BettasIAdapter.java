package com.skhlud.android.moad3;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by 1 on 12.05.2017.
 */

public class BettasIAdapter extends ArrayAdapter<double []>{

    private Context context;
    private ArrayList<double []> items;

    //constructor, call on creation
    public BettasIAdapter(Context context, int resource, ArrayList<double []> objects) {
        super(context, resource, objects);

        this.context = context;
        this.items = objects;
    }

    //called when rendering the list
    public View getView(int position, View convertView, ViewGroup parent) {

        double [] item = items.get(position);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        View view;
        view = inflater.inflate(R.layout.betta_i, null);
        TextView B = (TextView) view.findViewById(R.id.B);
        TextView I = (TextView) view.findViewById(R.id.I);
        B.setText(B.getText()+" "+item[0]);
        I.setText(I.getText()+" "+item[1]);
        if (item[0]==ModelBuilder.betaorH_Optimal)
        {
            (view.findViewById(R.id.layout)).setBackground(context.getResources().getDrawable(R.drawable.mincustomborder));
        }

        return view;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public double [] getItem(final int position) {
        return this.items.get(position);
    }
}
