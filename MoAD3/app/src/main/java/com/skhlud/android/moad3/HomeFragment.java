package com.skhlud.android.moad3;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


public class HomeFragment extends Fragment implements View.OnClickListener {

    Activity  mainactivity;
    ProgressBar  bar;
    int SampleSize ;
    double StartingDot;
    double Delta ;
    double MathExp ;
    double Disp ;
    int BorH ;
    int Core ;
    int SearchMethod ;
    String Function;
    Button button;
    TextView result_text;
    static MyTask task;
    boolean isTaskActive = false;
    Toast toast;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);
        return view;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mainactivity = getActivity();
        result_text = ((TextView) mainactivity.findViewById(R.id.Result));
        bar = (ProgressBar) mainactivity.findViewById(R.id.Progress);
        button = (Button) (mainactivity.findViewById(R.id.DoTheThing));
        button.setOnClickListener(this);
        if (isTaskActive)
        {
            result_text.setText("Подожди...");
            bar.setVisibility(View.VISIBLE);
            bar.setMax(SampleSize * 3);
            button.setEnabled(false);
        }
        if (task == null) {
            task = new MyTask();
        }
        if (ModelBuilder.isDone) {
            result_text.setText(String.valueOf("Расчет закончен!\nBettaОптимальная = " + ModelBuilder.betaorH_Optimal));
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.DoTheThing:
                DoTheThing();
                break;
        }
    }

    public void DoTheThing() {
        try {
            SampleSize = Integer.parseInt(((EditText) mainactivity.findViewById(R.id.SampleSize)).getText().toString());
            StartingDot = Double.parseDouble(((EditText) mainactivity.findViewById(R.id.StartingDot)).getText().toString());
            Delta = Double.parseDouble(((EditText) mainactivity.findViewById(R.id.Delta)).getText().toString());
            MathExp = Double.parseDouble(((EditText) mainactivity.findViewById(R.id.MathExp)).getText().toString());
            Disp = Double.parseDouble(((EditText) mainactivity.findViewById(R.id.Disp)).getText().toString());
            Core = ((Spinner) mainactivity.findViewById(R.id.Core)).getSelectedItemPosition();
            //BorH = ((Spinner) mainactivity.findViewById(R.id.BorH)).getSelectedItemPosition();
            SearchMethod = ((Spinner) mainactivity.findViewById(R.id.SearchMethod)).getSelectedItemPosition();
            Function =((EditText) mainactivity.findViewById(R.id.Function)).getText().toString();
            bar.setVisibility(View.VISIBLE);
            bar.setMax(SampleSize * 3);
            bar.setProgress(0);
            if (task == null)
                task = new MyTask();
            button.setEnabled(false);
            task = new MyTask();
            task.execute();
        }
        catch (Exception ex)
        {
            Toast toast = Toast.makeText(getContext(), ex.getMessage() + "\n" + ex.getLocalizedMessage(), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    class MyTask extends AsyncTask<Integer, Integer, String> {
        String Error;
        @Override
        protected String doInBackground(Integer... params) {
            try {
                ModelBuilder.DotheThing(SampleSize, StartingDot, Delta, MathExp, Disp, Core,0,SearchMethod, Function, this);
            }
            catch (Exception ex)
            {
                Error = ex.getMessage()+"\n"+ex.getLocalizedMessage();
            }
            return "Task Completed.";
        }

        @Override
        protected void onPreExecute() {
            isTaskActive = true;
            result_text.setText("Подожди...");
        }

        @Override
        protected void onPostExecute(String result) {
            isTaskActive = false;
            bar.setVisibility(View.GONE);
            if (ModelBuilder.isDone){
            result_text.setText(String.valueOf("Расчет закончен!\nBettaОптимальная = " + ModelBuilder.betaorH_Optimal));
            toast = Toast.makeText(mainactivity,
                    "Готово!", Toast.LENGTH_SHORT);}
            else
            {
                toast = Toast.makeText(mainactivity, Error
                        , Toast.LENGTH_SHORT);
                toast.show();
            }
            button.setEnabled(true);
            toast.show();
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            bar.setProgress(bar.getProgress() + values[0]);
        }
    }
}
